# SQL challenge

## Requirements
* have a working installation of Docker
* have [pipenv](https://github.com/pypa/pipenv) installed

## Setup
* clone the repo
* install the requirements via `pipenv install`
* start the docker container by running `docker-compose up` (You prob. want to do this in a separate shell)
* run `pipenv shell` to enter a virtualenv based shell where all the necessary command line tools are available
* within the pipenv shell you can now load the seed data by running `dbt seed --profiles-dir .`. This will create the necessary tables in the `dwh` schema which are relevant for solving this challenge.

## Answering the questions
Preferably you use DBT models to answer the questions. But you can also just provide us with plain SQL files that contain the answers to the question.
If you want to use DBT either use the `compile` command to see the full query or use the `run` command for example:
`dbt run --profiles-dir . --models question_1`

## Links
* [DBT documentation](https://docs.getdbt.com/)
