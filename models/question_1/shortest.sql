with aggregated_purchases as(
  select
    user_id,
    purchase_date,
    count(distinct purchase_id) as num_purchases
  from {{ref('fact_purchases')}}
  group by 1, 2
)

select
  count(distinct u.user_id) as num_buyers
from {{ref('fact_users')}} as u
  left join aggregated_purchases as p using(user_id)
where
  p.purchase_date = '2018-01-02'
  and p.num_purchases > 0
